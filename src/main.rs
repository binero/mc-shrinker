#[macro_use]
extern crate lazy_static;

use anvil_region::position::RegionPosition;
use anvil_region::region::Region;
use anyhow::{anyhow, bail, Result};
use clap::{AppSettings, Clap};
use log::{error, info, trace, warn};
use nbt::CompoundTag;
use regex::Regex;
use std::fs::File;
use std::io::BufReader;
use std::io::Read;
use std::io::Seek;
use std::path::Path;
use std::path::PathBuf;

#[derive(Clap)]
#[clap(version = "1.0.0", author = "Jeroen Bollen <contact@jeroenbollen.eu>")]
#[clap(about = "Delete regions that players have spent little to no time in.")]
#[clap(setting = AppSettings::ColoredHelp)]
struct CliOpts {
    #[clap(about = "The location of the region folder to shrink.")]
    region_folder: PathBuf,
    #[clap(
        short,
        long,
        default_value = "1",
        about = "The minimum amount of ticks a chunk must have been inhabited by a player to keep the chunk."
    )]
    min_ticks: i64,
}

fn main() -> Result<()> {
    env_logger::init();
    let cli_opts = CliOpts::parse();

    for region_file in std::fs::read_dir(cli_opts.region_folder)? {
        match region_file {
            Err(error) => warn!("Invalid path: {}", error),
            Ok(region_file) => {
                let path = region_file.path();
                let display_path = path.display();
                let filename = path.file_name().unwrap().to_string_lossy();
                trace!("Investigating file: {}", display_path);
                let region = match get_region(&path) {
                    Ok(region) => region,
                    Err(error) => {
                        error!("{}", error);
                        break;
                    }
                };

                let keep = match judge_region(region, cli_opts.min_ticks) {
                    Ok(true) => {
                        info!("Keeping region: {}", filename);
                        true
                    }
                    Ok(false) => {
                        warn!("Discarding region: {}", filename);
                        false
                    }
                    Err(error) => {
                        error!("Error in chunk {}: {}", filename, error);
                        true
                    }
                };

                if !keep {
                    std::fs::remove_file(path)?;
                }
            }
        }
    }

    Ok(())
}

fn get_region(path: &Path) -> Result<Region<impl Read + Seek>> {
    let position = filename_to_region_position(path)?;
    let file = File::open(path)?;
    let buffered_reader = BufReader::new(file);

    let region = Region::load(position, buffered_reader)?;
    Ok(region)
}

fn filename_to_region_position(filename: &Path) -> Result<RegionPosition> {
    let filename = match filename.to_str() {
        Some(filename) => filename,
        None => bail!("File '${}' contains invalid unicode", filename.display()),
    };

    lazy_static! {
        static ref REGEX: Regex = Regex::new("r\\.(-?\\d+)\\.(-?\\d+)\\.mca")
            .expect("Region filename regex failed to compile.");
    }

    if let Some(captures) = REGEX.captures(&filename) {
        let x: i32 = captures.get(1).unwrap().as_str().parse().unwrap();
        let z: i32 = captures.get(2).unwrap().as_str().parse().unwrap();

        Ok(RegionPosition::new(x, z))
    } else {
        bail!("Invalid region filename.")
    }
}

fn judge_region(region: Region<impl Read + Seek>, min_ticks: i64) -> Result<bool> {
    for chunk in region {
        if judge_chunk(chunk, min_ticks)? {
            return Ok(true);
        }
    }

    Ok(false)
}

fn judge_chunk(chunk: CompoundTag, min_ticks: i64) -> Result<bool> {
    let level = chunk
        .get_compound_tag("Level")
        .map_err(|_| anyhow!("Level tag not found in chunk"))?;
    let inhabited_for_ticks = level
        .get_i64("InhabitedTime")
        .map_err(|_| anyhow!("Level.InhabitedTime tag not found in chunk"))?;

    Ok(inhabited_for_ticks >= min_ticks)
}
