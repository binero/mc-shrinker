# MC-SHRINKER

Shrinks Minecraft world saves by deleting regions which have seen little player activity.

## Usage

```bash
# Usage help
cargo run --release -- --help

# Delete regions where players have spent under 30 seconds in every chunk.
cargo run --release -- --min-ticks 600 "world/region"
```
